<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

	public function __construct()
	{
		//load model terkait
		parent:: __construct();
		$this->load->model("barang_model");
		$this->load->model("jenis_barang_model");
		
		//load validasi
		$this->load->library('form_validation');
		
		//cek sesi login
		$user_login = $this->session->userdata();
		if (count($user_login)<= 1) {
			redirect("auth/index", "refresh");
			}
		
	}
	public function index()
	{
		$this->listBarang();
	}
	public function listBarang()
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDataJenis_barang();
		$data['data_barang'] = $this->barang_model->tampilDataBarang2();
		$data['content']		= 'forms/list_barang';
		$this->load->view('home', $data);
	}
	
	
	public function input_barang()
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDatajenis_barang();
		$data['content']		= 'forms/input_barang';
			/*if (!empty($_REQUEST)) {
				$m_barang = $this->barang_model;
				$m_barang->save();
				redirect("barang/index", "refresh");
			}
			*/
			
		$validation = $this->form_validation;
		$validation->set_rules($this->barang_model->rules());
		
		if ($validation->run()) {
			$this->barang_model->save();
			$this->session->set_flashdata('info', '<div style="color: white">Simpan Data Berhasil !</div>');
			redirect("barang/index", "refresh");
			}
		
		//$data['content']		= 'forms/input_barang';
		$this->load->view('home', $data);
	}
	public function detailBarang($kode_barang)
	{
		$data['detail_barang'] = $this->barang_model->detail($kode_barang);
		$data['content']		= 'forms/detail_barang';
		$this->load->view('home', $data);	
	}
	
	public function editBarang($kode_barang)
	{	
		$data['data_jenis_barang']		= $this->jenis_barang_model->tampilDataJenis_barang();
		$data['detail_barang']			= $this->barang_model->detail($kode_barang);
		$data['content']		= 'forms/edit_barang';
		
		
		/*if (!empty($_REQUEST)) {
				$m_barang = $this->barang_model;
				$m_barang->update($kode_barang);
				redirect("barang/index", "refresh");*/	
			
		$validation = $this->form_validation;
		$validation->set_rules($this->barang_model->rules());
		
		if ($validation->run()) {
			$this->barang_model->update($kode_barang);
			$this->session->set_flashdata('info', '<div style="color: white">Update Data Berhasil !</div>');
			redirect("barang/index", "refresh");
			}
		
		$this->load->view('home', $data);	
	}
	public function delete($kode_barang)
	{
		$m_barang = $this->barang_model;
		$m_barang->delete($kode_barang);	
		$this->session->set_flashdata('info', '<div style="color: white">Hapus Data Berhasil !</div>');
		redirect("barang/index", "refresh");	
	}
	
	
	
}
