<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends CI_Controller {
	
	public function __construct()
	{
		parent:: __construct();
		$this->load->model("karyawan_model");
		$this->load->model("jabatan_model");
		
		//load validasi
		$this->load->library('form_validation');
		
		//cek sesi login
		$user_login = $this->session->userdata();
		if (count($user_login)<= 1) {
			redirect("auth/index", "refresh");
			}
		
	}
	public function index()
	{
		$this->listKaryawan();
	}
	public function listKaryawan()
	{
		$data['data_karyawan'] 	= $this->karyawan_model->tampilDataKaryawan();
		$data['content']		= 'forms/list_karyawan';
		$this->load->view('home', $data);
	}
	public function input_karyawan()
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		$data['content']		= 'forms/input_karyawan';
			/*if (!empty($_REQUEST)) {
				$m_karyawan = $this->karyawan_model;
				$m_karyawan->save();
				redirect("karyawan/index", "refresh");	
			}*/
		$validation = $this->form_validation;
		$validation->set_rules($this->karyawan_model->rules());
		
		if ($validation->run()) {
			$this->karyawan_model->save();
			$this->session->set_flashdata('info', '<div style="color: white">Simpan Data Berhasil !</div>');
			redirect("karyawan/index", "refresh");
			}
		
		$this->load->view('home', $data);
	}
	public function detailKaryawan($nik)
	{
		$data['detail_karyawan'] = $this->karyawan_model->detail($nik);
		$data['content']		= 'forms/detail_karyawan';
		$this->load->view('home', $data);	
	}
	public function editKaryawan($nik)
	{	
		$data['data_jabatan'] 				= $this->jabatan_model->tampilDataJabatan();
		$data['detail_karyawan']			= $this->karyawan_model->detail($nik);
		$data['content']					= 'forms/edit_karyawan';
		
		/*if (!empty($_REQUEST)) {
				$m_karyawan = $this->karyawan_model;
				$m_karyawan->update($nik);
				redirect("karyawan/index", "refresh");	
	}*/
		$validation = $this->form_validation;
		$validation->set_rules($this->karyawan_model->rules());
		
		if ($validation->run()) {
			$this->karyawan_model->update($nik);
			$this->session->set_flashdata('info', '<div style="color: white">Update Data Berhasil !</div>');
			redirect("Karyawan/index", "refresh");
			}
		
		$this->load->view('home', $data);	
	}
	
	public function delete($nik)
	{
		$m_karyawan = $this->karyawan_model;
		$m_karyawan->delete($nik);
		$this->session->set_flashdata('info', '<div style="color: white">Hapus Data Berhasil !</div>');	
		redirect("karyawan/index", "refresh");	
	}
	
}
