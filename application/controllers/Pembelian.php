<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian extends CI_Controller {
	
	public function __construct()
	{
		parent:: __construct();
		$this->load->model("pembelian_model");
		$this->load->model("supplier_model");
		$this->load->model("barang_model");
	}
	
	public function index()
	{
		$this->listPembelian();
	}
	public function listPembelian()
	{
		$data['data_pembelian'] = $this->pembelian_model->tampilDataPembelian();
		$data['content']		= 'forms/list_pembelian';
		$this->load->view('home', $data);
	}

	public function input_pembelian()
	{	
		$data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
		$data['content']		= 'forms/input_pembelian';
		
		/*if (!empty($_REQUEST)) {
				$m_pembelian_h = $this->pembelian_model;
				$m_pembelian_h->savePembelianHeader();
				$id_terakhir = array();
				
				$id_terakhir = $m_pembelian_h->idTransaksiTerakhir();
				
				redirect("pembelian/input_pembelian_detail/" . $id_terakhir, "refresh");	
			}
		*/
		$validation = $this->form_validation;
		$validation->set_rules($this->pembelian_model->rules_header());
		
		if ($validation->run()) {
			$m_pembelian_h = $this->pembelian_model;
			$id_terakhir = array();
			$id_terakhir = $m_pembelian_h->idTransaksiTerakhir();
			
			$this->pembelian_model->savePembelianHeader();
			$this->session->set_flashdata('info', '<div style="color: white">Simpan Data Berhasil !</div>');
				
			redirect("pembelian/input_pembelian_detail/" . $id_terakhir, "refresh");	
			}
		
		$this->load->view('home', $data);
	}
	
	public function input_pembelian_detail($id_pembelian_header)
	
	{	
		// panggil data barang untuk form input		
		$data['data_barang'] 	= $this->barang_model->tampilDataBarang();
		$data['id_header']		= $id_pembelian_header;
		$data['data_pembelian_detail'] = $this->pembelian_model->tampildataPembelianDetail($id_pembelian_header);
		$data['content']		= 'forms/input_pembelian_detail';
		
		// proses simpan ke pembalian detail jika request from
			/*if (!empty($_REQUEST)) {
			// save detail
			$this->pembelian_model->savePembelianDetail($id_pembelian_header);
			
			//proses update stok
			
			$kode_barang	=$this->input->post('kode_barang');
			$qty		=$this->input->post('qty');
			$this->barang_model->updateStok($kode_barang, $qty);
			
			redirect("pembelian/input_pembelian_detail/" . $id_pembelian_header, "refresh");
			}*/
		$validation = $this->form_validation;
		$validation->set_rules($this->pembelian_model->rules_detail());
		
		if ($validation->run()) {
			$this->pembelian_model->savePembelianDetail($id_pembelian_header);
			$this->session->set_flashdata('info', '<div style="color: white">Simpan Data Berhasil !</div>');
			
			$kode_barang	=$this->input->post('kode_barang');
			$qty		=$this->input->post('qty');
			$this->barang_model->updateStok($kode_barang, $qty);
			
			redirect("pembelian/input_pembelian_detail/" . $id_pembelian_header, "refresh");	
			}
		
		$this->load->view('home', $data);
	}
	
	public function delete($id_pembelian_h)
	{
		$m_pembelian = $this->pembelian_model;
		$m_pembelian->delete($id_pembelian_h);
		$this->session->set_flashdata('info', '<div style="color: white">Hapus Data Berhasil !</div>');	
		redirect("pembelian/index", "refresh");	
	}
}
