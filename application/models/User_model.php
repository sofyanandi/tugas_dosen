<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{
	//panggil nama table
	private $_table = "user";
	
	public function cekUser($username,$password)
	{
		$this->db->select('nik,email,tipe');
		$this->db->where('nik',$username);
		$this->db->where('password',md5($password));
		$this->db->where('flag',1);
		$result = $this->db->get($this->_table);
		
		return $result->row_array();	
	}
	
}
