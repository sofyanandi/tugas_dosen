<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan_model extends CI_Model 
{
	private $_table = "karyawan";
	
	public function tampilDataKaryawan()
	{
		return $this->db->get($this->_table)->result();
	}
	public function tampilDataKaryawan2()
	{	
		$query =$this->db->query("SELECT * FROM karyawan WHERE flag = 1");
		return $query->result();
	}
	public function tampilDataKaryawan3()
	{
		$this->db->select('*');
		$this->db->order_by('nik','ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function detail($nik)
	{
		$this->db->select('*');
		$this->db->where ('nik', $nik);
		$this->db->where('flag',1);
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	public function save ()
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabung = $thn . "-" . $bln . "-" . $tgl;
		
		$data['nik']					= $this->input->post('nik');
		$data['nama_lengkap']			= $this->input->post('nama_karyawan');
		$data['tempat_lahir']			= $this->input->post('tempat_lahir');
		$data['tgl_lahir']				= $tgl_gabung;
		$data['jenis_kelamin']			= $this->input->post('jk');
		$data['alamat']					= $this->input->post('alamat');
		$data['telp']					= $this->input->post('telp');
		$data['kode_jabatan']			= $this->input->post('kode_jabatan');
		$data['flag']					= 1;
		$this->db->insert($this->_table, $data);
	}
	
	public function update($nik)
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabung = $thn . "-" . $bln . "-" . $tgl;
		
		$data['nama_lengkap']			= $this->input->post('nama_karyawan');
		$data['tempat_lahir']			= $this->input->post('tempat_lahir');
		$data['tgl_lahir']				= $tgl_gabung;
		$data['jenis_kelamin']			= $this->input->post('jenis_kelamin');
		$data['alamat']					= $this->input->post('alamat');
		$data['telp']					= $this->input->post('telp');
		$data['kode_jabatan']			= $this->input->post('kode_jabatan');
		$data['flag']					= 1;
		
		$this->db->where('nik', $nik);
		$this->db->update($this->_table, $data);
	}
	
	public function delete($nik)
	
	{
		$this->db->where('nik',$nik);
		$this->db->delete($this->_table);
	}
	
		public function rules()
	{
	
		return[
			[
				'field'  	=> 'nik',
				'label'  	=> 'NIK',
				'rules'  	=> 'required',
				'errors'	=> [
									'required'	=> 'Kode Barang tidak boleh kosong. '
							]
			],
			
			[
				'field'  	=> 'nama_karyawan',
				'label'  	=> 'Nama Karyawan',
				'rules'  	=> 'required',
				'errors'	=> ['required'	=> 'Nama Karyawan tidak boleh kosong. ']
			],
			
			[
				'field'  	=> 'tempat_lahir',
				'label'  	=> 'Tempat Lahir',
				'rules'  	=> 'required',
				'errors'	=> ['required'	=> 'Tempat Lahir tidak boleh kosong. ']
			],
			
			[
				'field'  	=> 'telp',
				'label'  	=> 'Telepon',
				'rules'  	=> 'required',
				'errors'	=> ['required'	=> 'Nomer Telepon tidak boleh kosong. ']
			],
			
			[
				'field'  	=> 'alamat',
				'label'  	=> 'Alamat',
				'rules'  	=> 'required',
				'errors'	=> ['required'	=> 'Alamat tidak boleh kosong. ']
			]
			
			/*[
				'field'  	=> 'stok',
				'label'  	=> 'Stok Barang',
				'rules'  	=> 'required|numeric',
				'errors'	=> ['required'	=> 'Stok Barang tidak boleh kosong. ' , 'numeric' => 'Stok Barang Harus Angka. ' ]
			],*/
			
		];
	
	}
}